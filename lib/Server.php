<?php

require_once 'Curl.php';

class Server 
{
    const ACTION_SELECT = 'select';
    const ACTION_INSERT = 'insert';
    const ACTION_EDIT   = 'edit';
    const ACTION_DELETE = 'delete';

    /**
     * @var Curl
     */
    private $_curl;

    /**
     * @var string
     */
    private $_host;

    /**
     * @var string
     */
    private $_dbDirCloudPath;

    /**
     * @var string
     */
    private $_dbFileName;

    /**
     * @var DbEngine
     */
    private $_dbEngine;


    private function _loadDbFromCloud()
    {
        $curl = $this->_curl;

        $url = $this->_host . '/Shared/tasks.txt';

        $curl->setOptionOnce( CURLOPT_URL, $url );
        $curl->setOptionOnce( CURLOPT_CUSTOMREQUEST, 'GET' );

        $response = $curl->exec();

        if ( $curl->getErrorNumber() ) {
            throw new Exception( $curl->getErrorText() );
        }

        $responseHeadersSize = curl_getinfo( $curl->getHandle(), CURLINFO_HEADER_SIZE );
        //$responseHeaders = substr( $response, 0, $responseHeadersSize );
        $responseBody    = substr( $response, $responseHeadersSize );

        //$this->_lastResponseHeaders = $responseHeaders;

        return $responseBody;
    }

    private function _uploadDbToCloud( $dbText )
    {
        $curl = $this->_curl;

        $url = $this->_host . '/' . $this->_dbDirCloudPath . '/' . $this->_dbFileName;

        $curl->setOptionOnce( CURLOPT_URL, $url );
        $curl->setOptionOnce( CURLOPT_CUSTOMREQUEST, 'PUT' );

        //$filePath = BASE_PATH . '/tmp/' . $this->_dbFileName;
        //$body = file_get_contents( $filePath );

        $curl->setOptionOnce(
            CURLOPT_HEADER,
            array(
                'Etag: ' . md5( $dbText ),//md5_file( $filePath ),
                'Sha256: ' . hash( 'sha256', $dbText ),
                'Content-length: ' . strlen( $dbText ),//filesize( $filePath ),
                'Expect: 100-continue',
                'Content-Type: application/binary',
            )
        );
        $curl->setOptionOnce( CURLOPT_POSTFIELDS, $dbText );

        $response = $curl->exec();

        if ($curl->getErrorNumber()) {
            throw new Exception( $curl->getErrorText() );
        }

        $responseHeadersSize = curl_getinfo($curl->getHandle(), CURLINFO_HEADER_SIZE);
        //$responseHeaders = substr($response, 0, $responseHeadersSize);
        $responseBody = substr($response, $responseHeadersSize);

        return $responseBody;
    }

    public function selectAction()
    {
        $data = $this->_dbEngine->getData();

        $types = array(
            array( 'id' => 1, 'name' => 'type1' ),
            array( 'id' => 2, 'name' => 'type2' ),
            array( 'id' => 3, 'name' => 'type3' ),
        );

        $json = $this->makeResponse( false, '', array( 'items' => $data, 'types' => $types, ) );

        return $json;
    }

    public function insertAction()
    {
        $task = ! empty( $_REQUEST['task'] ) ? $_REQUEST['task'] : null;

        if ( ! is_array( $task ) ) {
            throw new Exception('Task parameter must be an array');
        }
        if ( ! isset( $task['task_name'] )
            || ! isset( $task['progress'] )
            || ! isset( $task['type_id'] )
        ) {
            throw new Exception('Not all mandatory fields in \'task\' parameter');
        }

        //Filter redundant fields
        $newRow = array(
            'id' => md5( $task['task_name'] ),
            'order' => '',
            'task_name' => $task['task_name'],
            'description' => '',
            'progress' => $task['progress'],
            'parent_name' => '',
            'type_id' => $task['type_id'],
        );
        
        $data = $this->_dbEngine->getData();

        $data[] = $newRow;
        $dbText = $this->_dbEngine->encodeData( $data );
        
        $responseMessage = $this->_uploadDbToCloud( $dbText );

        $response = $this->makeResponse( false, $responseMessage, array( 'item' => $newRow ) );

        return $response;
    }

    public function editAction()
    {
        $task = ! empty( $_REQUEST['task'] ) ? $_REQUEST['task'] : null;
        if ( ! is_array( $task ) ) {
            throw new Exception('Task parameter must be an array');
        }
        if ( ! isset( $task['task_name'] )
            || ! isset( $task['progress'] )
            || ! isset( $task['type_id'] )
        ) {
            throw new Exception('Not all mandatory fields in \'task\' parameter');
        }

        $edit = false;
        $data = $this->_dbEngine->getData();
        foreach ( $data as $key => $row ) {
            if ( $data[$key]['id'] == $task['id'] ) {
                $data[$key]['task_name'] = $task['task_name'];
                $data[$key]['progress'] = $task['progress'];
                $data[$key]['type_id'] = $task['type_id'];

                $edit = $data[$key];
            }
        }

        $dbText = $this->_dbEngine->encodeData( $data );

        $responseMessage = $this->_uploadDbToCloud( $dbText );

        $response = $this->makeResponse( false, $responseMessage, array( 'item' => $edit ) );

        return $response;
    }

    public function deleteAction()
    {
        $taskId = ! empty( $_REQUEST['task_id'] ) ? $_REQUEST['task_id'] : null;
        if ( ! $taskId ) {
            throw new Exception('No task id was given');
        }

        $data = $this->_dbEngine->getData();
        $isDeleted = false;
        foreach ( $data as $key => $row ) {
            if ( $row['id'] == $taskId ) {
                unset($data[$key]);
                $isDeleted = true;
            }
        }

        if ( $isDeleted ) {
            $dbText = $this->_dbEngine->encodeData($data);
            $responseMessage = $this->_uploadDbToCloud($dbText);

            $response = $this->makeResponse( false, $responseMessage );
        } else {
            $response = $this->makeResponse( true, 'No record with such id');
        }

        return $response;
    }

    /**
     * Returns encoded response
     *
     * @param   bool    $isError
     * @param   string  $message
     * @param   array   $params
     *
     * @return  string
     */
    public function makeResponse( $isError, $message, $params = array() )
    {
        $responseArray = array(
            'error' => $isError,
            'message' => $message,
        );

        $responseArray = array_merge( $responseArray, $params );

        $json = json_encode( $responseArray );

        if ( json_last_error() ) {
            $json = json_encode( array(
                'error' => true,
                'message' => json_last_error_msg(),
            ) );
        }

        return $json;
    }

    /**
     * Constructor
     * 
     * @throws Exception
     */
    public function __construct()
    {
        $this->_host = 'https://webdav.yandex.ru';
        $this->_dbDirCloudPath = 'Shared';
        $this->_dbFileName = 'tasks.txt';
        $this->_dbEngine = new DbEngine();
        $this->_curl = new Curl();

        $curl = $this->_curl;

        //$curl->setAutoCloseSession( false );

        $curl->setOptionPermanent(
            CURLOPT_HTTPHEADER,
            array(
                'Depth: 0',
                'Authorization: Basic ' . base64_encode( 'api-gate:gf74SDh8d+0)5.c/31' ),
            )
        );
        $curl->setOptionPermanent( CURLOPT_HEADER, 1 );
        $curl->setOptionPermanent( CURLOPT_POST, 1 );

        $db = $this->_loadDbFromCloud();
        $db = str_replace( "\r", '', $db );

        $data = $this->_dbEngine->decodeData( $db );
        $this->_dbEngine->setData( $data );
    }

}