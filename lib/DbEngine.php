<?php

/**
 * Class DbEngine
 */
class DbEngine
{
    const OPEN_SIGN  = '<##';
    const CLOSE_SIGN = '##>';
    const FIELD_DELIMITER = '---';

    private $_data;

    private $_fieldNames = array(
        'id',
        'order',
        'task_name',
        'description',
        'progress',
        'parent_name',
        'type_id',
    );

    private function _getDataFormatPattern()
    {
        $regexp = '/' . preg_quote( self::OPEN_SIGN ) . '\s*';
        foreach ( $this->_fieldNames as $name ) {
            // ?P<...> means group by name
            $regexp .= preg_quote( self::FIELD_DELIMITER ) . preg_quote( $name )
                . '(?P<' . preg_quote( $name ) . '>.*)';
        }
        $regexp .= '\s*' . preg_quote( self::CLOSE_SIGN ) . '/sU';

        return $regexp;
    }

    public function setData( $data )
    {
        $this->_data = $data;
    }

    public function getData()
    {
        return $this->_data;
    }

    /**
     *
     *
     * @param   string  $dbText
     *
     * @return  array
     */
    public function decodeData( $dbText )
    {
        $allMatches = array();

        $pattern = $this->_getDataFormatPattern();

        preg_match_all( $pattern, $dbText, $allMatches, PREG_SET_ORDER );

        $data = array();
        foreach ( $allMatches as $patternMatches ) {
            if ( count( $patternMatches ) ) {
                $rawData = array_intersect_key(
                    $patternMatches,
                    array_flip( $this->_fieldNames )
                );
                //$rawData['id'] = htmlspecialchars(base64_encode($rawData['name']));
                $data[] = array_map( 'trim', $rawData );
            }
        }


        return $data;
    }

    /**
     * Encodes data for saving in the db file
     *
     * @param   array   $items
     *
     * @return  string
     */
    public function encodeData( $items )
    {
        $dbText = '';

        foreach ( $items as $item ) {
            $dbText .= self::OPEN_SIGN . "\n";
            foreach ($this->_fieldNames as $fieldName) {
                    $dbText .= self::FIELD_DELIMITER . $fieldName . ' ' . $item[$fieldName] . "\n";
            }
            $dbText .= self::CLOSE_SIGN . "\n\n";
        }

        return $dbText;
    }



}