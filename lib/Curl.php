<?php

/**
 * cURL control object
 */
class Curl
{
    /**
     * Counter of curl connections
     *
     * @var int
     */
    private $_curlConnectionsCounter = 0;

    /**
     * Last connected host
     *
     * @var string
     */
    private $_lastHost = '';

    /**
     * Error text. If error occurred it is empty string.
     * (curl_error returns empty str if no error)
     *
     * @var string
     */
    private $_errorText = '';

    /**
     * Error number or 0 (zero) if no error occurred.
     * (curl_errno returns 0 if no error)
     *
     * @var int
     */
    private $_errorNumber = 0;

    /**
     * Current curl session handle.
     *
     * @var resource
     */
    private $_curlHandle;

    /**
     * If return transfer option is not set, this value will be used
     * to determine set it option by default or not
     *
     * @var bool
     */
    private $_defaultReturnTransfer;

    /**
     * If true session will be closed after every execution
     *
     * @var bool
     */
    private $_autoCloseSession;

    /**
     * Array with cURL options, which should be set
     * by curl_setopt_array ONLY FOR ONE connection.
     *
     * Array takes view like OPTION_CONSTANT => value.
     *
     * @var array
     */
    private $_optionsOnce = array();

    /**
     * Array with headers, which should be set
     * by curl_setopt_array FOR ALL FURTHER connections
     *
     * Array takes view like OPTION_CONSTANT => value.
     *
     * @var array
     */
    private $_optionsPermanent = array();

    /**
     * Returns last connected host
     *
     * @return  string
     */
    public function getLastHost()
    {
        return $this->_lastHost;
    }

    /**
     * Returns curl handle
     *
     * @return  resource
     *
     * @throws  \Exception  If cURL initialization failed
     */
    public function getHandle()
    {
        if ( ! $this->_curlHandle ) {
            $this->_curlHandle = curl_init();

            if ( ! $this->_curlHandle ) {
                throw new \Exception('cURL initialization failed');
            }
        }

        return $this->_curlHandle;
    }

    /**
     * Returns error text. Empty string, if no error
     *
     * @return  string
     */
    public function getErrorText()
    {
        return $this->_errorText;
    }

    /**
     * Returns the error number or 0 (zero) if no error occurred.
     *
     * @return  int
     */
    public function getErrorNumber()
    {
        return $this->_errorNumber;
    }

    /**
     * Returns count of the curl connections
     *
     * @return  int
     */
    public function getConnectionsCount()
    {
        return $this->_curlConnectionsCounter;
    }

    /**
     * Sets default-return-transfer flag that will be used
     * to determine set it option by default or not
     *
     * @param   bool    $value  True - always return transfer, otherwise false
     */
    public function setDefaultReturnTransfer( $value )
    {
        $this->_defaultReturnTransfer = (bool)$value;
    }

    /**
     * Returns default-return-transfer flag that will be used
     * to determine set it option by default or not
     *
     * @return  bool
     */
    public function getDefaultReturnTransferState()
    {
        return $this->_defaultReturnTransfer;
    }

    /**
     * Sets flag to determine close or not session after every execution
     *
     * @param   bool    $autoCloseSession
     */
    public function setAutoCloseSession( $autoCloseSession )
    {
        $this->_autoCloseSession = $autoCloseSession;
    }

    /**
     * Returns auto-closing session flag
     *
     * @return  bool
     */
    public function getAutoCloseSessionState()
    {
        return $this->_autoCloseSession;
    }

    /**
     * Adds option for once setting, to optionsOnce array
     *
     * @param   int     $optionConstant cURL option constant
     * @param   mixed   $optionValue    Value of the option
     */
    public function setOptionOnce( $optionConstant, $optionValue )
    {
        $this->_optionsOnce[$optionConstant] = $optionValue;
    }

    /**
     * Adds option for setting for permanent, to optionsPermanent array
     *
     * @param   int   $optionConstant cURL option constant
     * @param   mixed $optionValue    Value of the option
     */
    public function setOptionPermanent($optionConstant, $optionValue)
    {
        $this->_optionsPermanent[$optionConstant] = $optionValue;
    }

    /**
     * Unsets option from optionsPermanent array
     *
     * @param   int $optionNameConstant cURL option constant
     */
    public function unsetOptionPermanent($optionNameConstant)
    {
        unset($this->_optionsPermanent[$optionNameConstant]);
    }

    /**
     * Checks if option was appointed
     *
     * @param   int $optionNameConstant cURL option constant
     *
     * @return  bool
     */
    public function isOptionAppointed( $optionNameConstant )
    {
        if ( isset( $this->_optionsOnce[$optionNameConstant] )
            || isset( $this->_optionsPermanent[$optionNameConstant] )
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns already appointed curl option
     *
     * @param   int $optionNameConstant cURL option name constant
     *
     * @return  mixed
     */
    public function getAppointedOption( $optionNameConstant )
    {
        if ( isset( $this->_optionsOnce[$optionNameConstant] ) ) {
            return $this->_optionsOnce[$optionNameConstant];
        } elseif ( isset( $this->_optionsPermanent[$optionNameConstant] ) ) {
            return $this->_optionsPermanent[$optionNameConstant];
        }

        return null;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setDefaultReturnTransfer( true );
        $this->setAutoCloseSession( false );
    }

    /**
     * Performs a cURL session.
     * Return values just like in curl_exec.
     *
     * @return  string|bool
     */
    public function exec()
    {
        $this->_curlConnectionsCounter++;

        curl_reset( $this->getHandle() );

        if ( ! $this->isOptionAppointed( CURLOPT_RETURNTRANSFER )
            && $this->getDefaultReturnTransferState()
        ) {
            curl_setopt( $this->getHandle(), CURLOPT_RETURNTRANSFER, 1 );
        }

        curl_setopt_array( $this->getHandle(), $this->_optionsPermanent );
        curl_setopt_array( $this->getHandle(), $this->_optionsOnce );
        $this->_optionsOnce = array();

        $page = curl_exec( $this->getHandle() );

        $this->_lastHost = curl_getinfo( $this->getHandle(), CURLINFO_EFFECTIVE_URL );

        if ( $page === false ) {
            $this->_errorText   = curl_error( $this->getHandle() );
            $this->_errorNumber = curl_errno( $this->getHandle() );
        }

        if ( $this->getAutoCloseSessionState() ) {
            $this->close();
        }

        return $page;
    }

    /**
     * Close a cURL session
     */
    public function close()
    {
        if ( $this->getHandle() ) {
            curl_close( $this->getHandle() );
        }

        $this->_curlHandle = null;
    }

}
