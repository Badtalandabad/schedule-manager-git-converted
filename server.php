<?php

define('BASE_PATH', dirname(__FILE__));

require_once BASE_PATH . '/Curl.php';

ini_set( 'error_reporting', E_ALL );
ini_set( 'display_startup_errors', 1 );
ini_set( 'html_errors', 1 );
ini_set( 'display_errors', 1 );

$host = 'https://webdav.yandex.ru';

$curl = new Curl();

$curl->setAutoCloseSession(false);

$curl->setOptionPermanent(
    CURLOPT_HTTPHEADER,
    array(
        'Depth: 0',
        'Authorization: Basic ' . base64_encode( 'api-gate:gf74SDh8d+0)5.c/31' ),
    )
);
$curl->setOptionPermanent( CURLOPT_HEADER, 1 );
$curl->setOptionPermanent( CURLOPT_POST, 1 );

/*$body = '<D:propfind xmlns:D="DAV:">
  <D:prop>
    <D:quota-available-bytes/>
    <D:quota-used-bytes/>
  </D:prop>
</D:propfind>';*/

$url = $host . '/DB/loaded.txt';

$curl->setOptionOnce( CURLOPT_URL, $url );
$curl->setOptionOnce( CURLOPT_CUSTOMREQUEST, 'PUT' );

$filePath = BASE_PATH . '/loaded.txt';
//file_put_contents($filePath, "\n new line", FILE_APPEND);
$body = file_get_contents( $filePath );

$curl->setOptionOnce(
    CURLOPT_HEADER,
    array(
        'Etag: ' . md5_file( $filePath ),
        'Sha256: ' . hash( 'sha256', $body ),
        'Content-length: ' . filesize( $filePath ),
        'Expect: 100-continue',
        'Content-Type: application/binary',
    )
);
$curl->setOptionOnce( CURLOPT_POSTFIELDS, $body );

$response = $curl->exec();

if ($curl->getErrorNumber()) {
    echo $curl->getErrorText();
    return;
}

$responseHeadersSize = curl_getinfo($curl->getHandle(), CURLINFO_HEADER_SIZE);
$responseHeaders = substr($response, 0, $responseHeadersSize);
$responseBody    = substr($response, $responseHeadersSize);

//$fileSuccess = file_put_contents( BASE_PATH . '/loaded.txt', $responseBody );
//var_dump($fileSuccess);

var_dump($responseHeaders, $responseBody);

$curl->close();