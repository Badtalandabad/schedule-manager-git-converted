"use strict";

angular.module('myApp', [])
    .constant('MAX_LENGTH', 50)
    .constant('MIN_LENGTH', 2)
    .factory('helperFactory', function() {
        return {
            filterFieldArrayByDone : function(thisArray, thisField, thisValue) {
                var arrayToReturn = [];

                for (var i = 0; i < thisArray.length; i++) {
                    if (thisArray[i].done == thisValue) {
                        arrayToReturn.push(thisArray[i][thisField]);
                    }
                }
                return arrayToReturn;
            }
        };
    })
    .controller('TaskManagerController', function($scope, $http, $log, helperFactory, MAX_LENGTH, MIN_LENGTH){
        var urlSelect = '/app.php?action=select';
        var urlInsert = '/app.php?action=insert';
        var urlEdit   = '/app.php?action=edit';
        var urlRemove = '/app.php?action=remove';
        var urlDelete = '/app.php?action=delete';

        $scope.types = [];
        $scope.items = [];

        //$scope.item = '';
        //$scope.qty = '';

        $scope.newTaskName = '';
        $scope.newProgress = '';
        $scope.newTypeId = '';

        $scope.editOn = false;
        $scope.editTaskName = '';
        $scope.editProgress = '';
        $scope.editTypeId = '';

        $scope.turnOnEdit = function(item) {
            if (!item) {
                return false;
            }
            jQuery('.b-items-row__edit').slideUp();
            jQuery('.b-items-row__data').slideDown();
            $scope.editOn = true;

            $scope.editTaskName = item.task_name;
            $scope.editProgress = item.progress;
            $scope.editTypeId = item.type_id;

            jQuery('#item-data-' + item.id).slideUp();
            jQuery('#item-edit-' + item.id).slideDown();

            return false;
        };

        $scope.turnOffEdit = function(item) {
            jQuery('.b-items-row__edit').slideUp();
            jQuery('.b-items-row__data').slideDown();
            $scope.editOn = false;

            $scope.editTaskName = '';
            $scope.editProgress = '';
            $scope.editTypeId = '';

            return false;
        };

        $scope.saveEditedItemInScope = function(item) {
            for (var i = 0; i < $scope.items.length; i++) {
                if (item.id == $scope.items[i].id) {
                    $scope.items[i].task_name = $scope.editTaskName;
                    $scope.items[i].progress = $scope.editProgress;
                    $scope.items[i].type_id = $scope.editTypeId;
                }
            }
            //item.task_name = $scope.editTaskName;
            //item.progress = $scope.editProgress;
            //item.type_id = $scope.editTypeId;
        };

        $scope.getTypeNameById = function(id) {
            for (var i = 0; i < $scope.types.length; i++) {
                if ($scope.types[i].id == id) {
                    return $scope.types[i].name;
                }
            }
            return '';
        };

        $scope.howManyMoreCharactersNeeded = function() {
            var characters = (MIN_LENGTH - $scope.newTaskName.length);
            return (characters > 0) ? characters : 0;
        };

        $scope.howManyCharactersRemaining = function() {
            var characters = (MAX_LENGTH - $scope.newTaskName.length);
            return (characters > 0) ? characters : 0;
        };

        $scope.howManyCharactersOver = function() {
            var characters = (MAX_LENGTH - $scope.newTaskName.length);
            return (characters < 0) ? Math.abs(characters) : 0;
        };

        $scope.minimumCharactersMet = function() {
            return ($scope.howManyMoreCharactersNeeded() == 0);
        };

        $scope.anyCharactersOver = function() {
            return ($scope.howManyCharactersOver() > 0);
        };

        $scope.isNumberOfCharactersWithinRange = function() {
            return (
                $scope.minimumCharactersMet()
                && ! $scope.anyCharactersOver()
            );
        };

        $scope.goodToGo = function() {
            return (
                $scope.isNumberOfCharactersWithinRange()
                && parseInt($scope.newProgress) >= 0
                && parseInt($scope.newProgress) <= 100
                && $scope.newTaskName != ''
            );
        };

        $scope.clear = function() {
            $scope.newTaskName = '';
            $scope.newProgress = '';
        };

        function _recordAddedSuccessfully(data)
        {
            return data && !data.error && data.item;
        }

        $scope.insert = function() {
            if ($scope.goodToGo()) {
                $http({
                    method: 'POST',
                    url: urlInsert,
                    data: 'task[task_name]=' + $scope.newTaskName
                        + '&task[progress]=' + $scope.newProgress
                        + '&task[type_id]=' + $scope.newTypeId,
                    headers: {'Content-type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    if (_recordAddedSuccessfully(data)) {
                        $scope.items.push({
                            id: data.item.id,
                            task_name: data.item.task_name,
                            progress: data.item.progress,
                            type_id: data.item.type_id
                        });
                        $scope.clear();
                    }
                }).error(function(data, status, headers, config){
                    throw new Error('Something went wrong with inserting record');
                });
            }
        };

        $scope.select = function() {
            $http.get(urlSelect)
                .success(function(data){
                    if (data.items) {
                        $scope.items = data.items;
                    }
                    if (data.types) {
                        $scope.types = data.types;
                        $scope.newTypeId = $scope.types[0].id;
                    }
                }).error(function(data, status, headers, config){
                    throw new Error('Something went wrong with selection records');
                });
        };

        $scope.select();

        $scope.edit = function(item) {
            $scope.editTaskName = this.editTaskName;
            $scope.editProgress = this.editProgress;
            $scope.editTypeId = this.editTypeId;
            $scope.saveEditedItemInScope(item);
            $scope.turnOffEdit();
            $http({
                method: 'POST',
                url: urlEdit,
                data: 'task[id]=' + item.id
                    + '&task[task_name]=' + item.task_name
                    + '&task[progress]=' + item.progress
                    + '&task[type_id]=' + item.type_id,
                headers: {'Content-type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                $log.info(data);
            }).error(function(data, status, headers, config){
                throw new Error('Something went wrong with updating record');
            });
        };

        function _recordsRemovedSuccessfully(data)
        {
            return data && !data.error;
        }

        $scope.remove = function() {
            var removeIds = helperFactory.filterFieldArrayByDone($scope.items, 'id', 1);
            if (removeIds.length > 0) {
                $http({
                    method: 'POST',
                    url: urlRemove,
                    data: 'ids=' + removeIds.join('|'),
                    headers: {'Content-type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    if (_recordsRemovedSuccessfully(data)) {
                        $scope.items = $scope.items.filter(function(item){
                            return item.done == 0;
                        });
                    }
                }).error(function(data, status, headers, config){
                    throw new Error('Something went wrong with removing record');
                });
            }
        };

        $scope.delete = function(item) {
            $http({
                method: 'POST',
                url: urlDelete,
                data: 'task_id=' + item.id,
                headers: {'Content-type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                for (var i = 0; i < $scope.items.length; i++) {
                    if ($scope.items[i].id == item.id) {
                        $scope.items.splice(i, 1);
                    }
                }
            }).error(function(data, status, headers, config){
                throw new Error('Something went wrong with deleting record');
            });

        };

        $scope.print = function() {
            window.print();
        };

    });
