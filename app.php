<?php

//define('BASE_PATH', dirname(__FILE__));

require_once 'lib/Server.php';
require_once 'lib/DbEngine.php';
//require_once 'lib/Data.php';

$request = ! empty( $_REQUEST['action'] ) ? $_REQUEST['action'] : '';

$server = new Server();

$response = '';

try {
    switch ($request) {
        case $server::ACTION_SELECT:
            $response = $server->selectAction();
            break;
        case $server::ACTION_INSERT:
            $response = $server->insertAction();
            break;
        case $server::ACTION_EDIT:
            $response = $server->editAction();
            break;
            break;
        case $server::ACTION_DELETE:
            $response = $server->deleteAction();
            break;
        default:
            $response = 'Choose an action.';
    }
} catch (Exception $e) {
    //TODO: response place
    json_encode(
        array(
            'error' => false,
            'errorMessage' => $e->getMessage(),
        )
    );

}

echo $response;