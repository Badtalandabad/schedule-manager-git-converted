<?php

define('JSON_CONSTANT', JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

try {
    if (empty($_POST['id'])) {
        throw new PDOException('Invalid request');
    }

    $id = $_POST['id'];
    $done = empty($_POST['done']) ? 0 : 1;

    $db = new PDO('sqlite:../db/shoplist.db');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'UPDATE items SET done = ? WHERE id = ?';
    $statement = $db->prepare($sql);
    if (!$statement->execute(array($done, $id))) {
        throw new PDOException('The execution method failed');
    }

    echo json_encode(array(
        'error' => false,
    ), JSON_CONSTANT);

} catch (PDOException $e) {
    echo json_encode(array(
        'error' => true,
        'message' => $e->getMessage(),
    ), JSON_CONSTANT);
}