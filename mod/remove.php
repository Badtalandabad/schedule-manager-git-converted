<?php

define('JSON_CONSTANT', JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

try {
    if (empty($_POST['ids'])) {
        throw new PDOException('Invalid request');
    }

    $ids = $_POST['ids'];
    $idsArray = explode('|', $ids);
    $placeholders = implode(',', array_fill(0, count($idsArray), '?'));

    $db = new PDO('sqlite:../db/shoplist.db');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'DELETE FROM items WHERE id IN (' . $placeholders . ')';
    $statement = $db->prepare($sql);
    if (!$statement->execute($idsArray)) {
        throw new PDOException('The execution method failed');
    }

    echo json_encode(array(
        'error' => false,
    ), JSON_CONSTANT);

} catch (PDOException $e) {
    echo json_encode(array(
        'error' => true,
        'message' => $e->getMessage(),
    ), JSON_CONSTANT);
}