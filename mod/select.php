<?php

define('JSON_CONSTANT', JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

try {
    $db = new PDO('sqlite:../db/shoplist.db');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'SELECT items.*, types.name AS type_name'
        . ' FROM items'
        . ' JOIN types ON types.id = items.type'
        . ' ORDER BY items.date ASC';
    $result = $db->query($sql);
    if (!$result) {
        throw new PDOException('The result returned no object');
    }
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $items = $result->fetchAll();


    $sql = 'SELECT * FROM types ORDER BY id';
    $result = $db->query($sql);
    if (!$result) {
        throw new PDOException('The result returned no object');
    }
    $result->setFetchMode(PDO::FETCH_ASSOC);
    $types = $result->fetchAll();

    echo json_encode(array(
        'error' => false,
        'items' => $items,
        'types' => $types,
    ), JSON_CONSTANT);

} catch (PDOException $e) {
    echo json_encode(array(
        'error' => true,
        'message' => $e->getMessage(),
    ), JSON_CONSTANT);
}