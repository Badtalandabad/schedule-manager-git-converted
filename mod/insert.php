<?php

define('JSON_CONSTANT', JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

try {
    if (empty($_POST['item']) || empty($_POST['qty']) || empty($_POST['type'])) {
        throw new PDOException('Invalid request');
    }

    $item = $_POST['item'];
    $qty  = $_POST['qty'];
    $type = $_POST['type'];

    $db = new PDO('sqlite:../db/shoplist.db');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'INSERT INTO items (`item`,qty, `type` ) VALUES (?, ?, ?)';
    $statement = $db->prepare($sql);
    if (!$statement->execute(array($item, $qty, $type))) {
        throw new PDOException('The execution method failed');
    }

    $id = $db->lastInsertId();

    $sql = 'SELECT items.*, types.name AS type_name'
        . ' FROM items'
        . ' JOIN types ON types.id = items.type'
        . ' WHERE items.id = ?';
    $statement= $db->prepare($sql);
    if (!$statement->execute(array($id))) {
        throw new PDOException('The result returned no object');
    }
    $statement->setFetchMode(PDO::FETCH_ASSOC);

    $item = $statement->fetch();

    echo json_encode(array(
        'error' => false,
        'item' => $item,
    ), JSON_CONSTANT);

} catch (PDOException $e) {
    echo json_encode(array(
        'error' => true,
        'message' => $e->getMessage(),
    ), JSON_CONSTANT);
}